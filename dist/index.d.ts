import { ModuleWithProviders } from '@angular/core';
export * from './editor.component';
export declare class EditorModule {
    static forRoot(): ModuleWithProviders;
}
