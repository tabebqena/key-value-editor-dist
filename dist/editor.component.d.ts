import { OnInit, EventEmitter } from '@angular/core';
export declare class EditorComponent implements OnInit {
    object: any;
    metadata: any;
    usableArray: any[];
    usableJson: {};
    getUsableArray: EventEmitter<{}>;
    getUsableJson: EventEmitter<{}>;
    constructor();
    hasProp(obj: any, key: any): any;
    objectedited(): void;
    addProperty(): void;
    delete(prop: any): void;
    getKeyClass(prop: any): any;
    getTitleClass(): any;
    ngOnInit(): void;
}
