import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { BrowserModule } from '@angular/platform-browser';

import { EditorComponent } from './editor.component';

 
//import { SampleComponent } from './sample.component';
//import { SampleDirective } from './sample.directive';
//import { SamplePipe } from './sample.pipe';
//import { SampleService } from './sample.service';
export * from './editor.component';
//export * from './sample.component';
//export * from './sample.directive';
//export * from './sample.pipe';
//export * from './sample.service';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule
  ],
  declarations: [
    EditorComponent
  ],
  exports: [
     EditorComponent
  ]
})
export class EditorModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: EditorModule,
   //   providers: [SampleService]
    };
  }
}
